# 快捷键
- Ctrl + Alt + L（Mac为Command+Option +L）：代码格式化
- Ctrl + Alt + Shift + L（Mac为Command+Option+L）：整个文件格式化
- Ctrl+/（Mac为Command+/）：注释代码
- Alt + 7 / Ctrl + F12（Mac为Command+7）：快速打开代码结构树
- Ctrl+Q（Mac为F1）：查看Java接口文档

**查找**
- Ctrl + Shift + F：工程中全局查找