# APP工程结构
HarmonyOS 应用发布形态为 APP Pack（Application Package，简称APP），它是由一个或多个 HAP（HarmonyOS Ability Package）包
以及描述 APP Pack 属性的 pack.info 文件组成。
一个 HAP 在工程目录中对应一个 Module，它是由代码、资源、第三方库及应用清单文件组成，可以分为 `Entry` 和 `Feature` 两种类型。
- **Entry**：应用的主模块。一个APP中，对于同一设备类型必须有且只有一个entry类型的HAP，可独立安装运行。
- **Feature**：应用的动态特性模块。一个APP可以包含一个或多个feature类型的HAP，也可以不含。